package pl.sda.programming1.generic;

/**
 * TODO
 */
public class RunningShoes extends SportShoes {
    private int amortizationLevel;

    public RunningShoes(double weight, int flexibility, int amortizationLevel) {
        super(weight, flexibility);
        this.amortizationLevel = amortizationLevel;
    }

    public int getAmortizationLevel() {
        return amortizationLevel;
    }
}
