package pl.sda.programming1.generic;

/**
 * TODO
 */
public class Hills extends Shoes {
    private int dimension;

    public Hills(double weight, int dimension) {
        super(weight);
        this.dimension = dimension;
    }

    public int getDimension() {
        return dimension;
    }
}
