package pl.sda.programming1.generic;

class SportShoes extends Shoes {
    private int flexibility;

    public SportShoes(double weight, int flexibility) {
        super(weight);
        this.flexibility = flexibility;
    }

    public int getFlexibility() {
        return flexibility;
    }
}
