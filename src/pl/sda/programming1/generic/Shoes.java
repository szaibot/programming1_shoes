package pl.sda.programming1.generic;

/**
 * TODO
 */
public abstract class Shoes {
    private double weight;

    public Shoes(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }
}
