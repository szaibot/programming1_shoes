package pl.sda.programming1.generic;

/**
 * TODO
 */
class Person {
    private Shoes shoes;

    public Person(Shoes shoes) {
        this.shoes = shoes;
    }

    public Shoes getShoes() {
        return shoes;
    }
}
