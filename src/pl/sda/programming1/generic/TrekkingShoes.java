package pl.sda.programming1.generic;

/**
 * TODO
 */
public class TrekkingShoes extends SportShoes {
    private int waterproofLevel;

    public TrekkingShoes(double weight, int flexibility, int waterproofLevel) {
        super(weight, flexibility);
        this.waterproofLevel = waterproofLevel;
    }

    public int getWaterproofLevel() {
        return waterproofLevel;
    }
}
