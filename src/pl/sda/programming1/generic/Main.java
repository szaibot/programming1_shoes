package pl.sda.programming1.generic;

/**
 * TODO
 */
class Main {
    public static void main(String[] args) {
        Hills hills = new Hills(0.5, 5);
        Model model = new Model(hills);
        System.out.println("Model shoes weight:" + model.getShoes().getWeight());

        RunningShoes runningShoes = new RunningShoes(1, 7, 3);
        Runner runner = new Runner(runningShoes);
        System.out.println("Runner shoes weight:" + runner.getShoes().getWeight());

        TrekkingShoes trekkingShoes = new TrekkingShoes(5, 2, 10);
        Trekker trekker = new Trekker(trekkingShoes);
        System.out.println("Trekker shoes weight:" + trekker.getShoes().getWeight());
    }
}
